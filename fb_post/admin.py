from django.contrib import admin

from .models.comment import Comment
from .models.post import Post
from .models.reaction import Reaction
from .models.user import User

admin.site.register(User)
admin.site.register(Post)
admin.site.register(Comment)
admin.site.register(Reaction)
