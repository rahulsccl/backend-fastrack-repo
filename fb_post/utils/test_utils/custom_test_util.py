from django_swagger_utils.utils.test import CustomAPITestCase
from freezegun import freeze_time
import datetime
from fb_post.constants.exception_messsages import get_random_reaction
from fb_post.models.user import User
from fb_post.models.post import Post
from fb_post.models.comment import Comment
from fb_post.models.reaction import Reaction
from .get_user_posts import USER_POSTS


class TestFixtures(CustomAPITestCase):
    freeze = freeze_time("2012-01-14 12:00:01")
    USERS = [
                {"name": "Praveen", "profile_pic_url": "none", "password": "123"},
                {"name": "Mahendra", "profile_pic_url": "none", "password": "123"},
                {"name": "One", "profile_pic_url": "none", "password": "123"},
            ],
    POSTS = [
        {"user_id": 1, "description": "Nothing"},
        {"user_id": 1, "description": "Nothing"},
        {"user_id": 2, "description": "Nothidgerng"}
    ]
    COMMENTS = [
        {"user_id": 1, "post_id": 1, "description": "No COmment for you."},
        {"user_id": 1, "post_id": 2, "description": "No COmment for you."},
        {"user_id": 2, "post_id": 3, "description": "No COmment for you."},
        {"user_id": 3, "post_id": 2, "description": "No COmment for you."},
        {"user_id": 2, "post_id": 1, "description": "No COmment for you."}
    ]
    REPLIES = [
        {"user_id": 1, "parent_id": 1, "description": "No COmment for you."},
        {"user_id": 1, "parent_id": 2, "description": "No COmment for you."},
        {"user_id": 2, "parent_id": 3, "description": "No COmment for you."},
        {"user_id": 3, "parent_id": 2, "description": "No COmment for you."},
        {"user_id": 2, "parent_id": 1, "description": "No COmment for you."}
    ]
    POST_REACTIONS = [
        {"user_id": 1, "post_id": 1, "reaction_type": "LIKE"},
        {"user_id": 2, "post_id": 1, "reaction_type": "HAHA"},
        {"user_id": 1, "post_id": 1, "reaction_type": "ANGRY"},
        {"user_id": 3, "post_id": 2, "reaction_type": "SAD"},
        {"user_id": 2, "post_id": 2, "reaction_type": "LIKE"},
        {"user_id": 3, "post_id": 3, "reaction_type": "SAD"},
        {"user_id": 1, "post_id": 3, "reaction_type": "ANGRY"},
        {"user_id": 2, "post_id": 3, "reaction_type": "LIKE"},
    ]
    COMMENT_REACTIONS = [
        {"user_id": 1, "comment_id": 1, "reaction_type": "LIKE"},
        {"user_id": 1, "comment_id": 1, "reaction_type": "LOVE"},
        {"user_id": 2, "comment_id": 1, "reaction_type": "HAHA"},
        {"user_id": 1, "comment_id": 1, "reaction_type": "WOW"},
        {"user_id": 3, "comment_id": 2, "reaction_type": "SAD"},
        {"user_id": 1, "comment_id": 3, "reaction_type": "ANGRY"},
        {"user_id": 2, "comment_id": 2, "reaction_type": "LIKE"},
        {"user_id": 2, "comment_id": 3, "reaction_type": "ANGRY"},
        {"user_id": 2, "comment_id": 3, "reaction_type": "ANGRY"},
    ]

    def check_post_equality(self, db_post, api_post):
        self.assertEqual(db_post['post_id'], api_post['post_id'])
        self.assertEqual(db_post['post_content'], api_post['post_content'])
        self.assertEqual(db_post['reactions']['count'], api_post['reactions']['count'])
        self.assertListEqual(db_post['reactions']['type'], api_post['reactions']['type'])
        self.assertEqual(db_post['posted_by']['user_id'],
                         api_post['posted_by']['user_id'])
        self.assertEqual(db_post['posted_by']['name'], api_post['posted_by']['name'])

        api_comments = db_post['comments']
        db_comments = api_post['comments']

        for (api_comment, db_comment) in zip(api_comments, db_comments):
            self.check_comment_equality(api_comment, db_comment)
            self.assertEqual(api_comment['replies_count'], db_comment['replies_count'])

            api_replies = api_comment['replies']
            db_replies = db_comment['replies']

            for (api_reply, db_reply) in zip(api_replies, db_replies):
                self.check_comment_equality(api_reply, db_reply)

    def check_comment_equality(self, api_comment, db_comment, with_reaction=True):
        self.assertEqual(api_comment['comment_id'], db_comment['comment_id'])
        self.assertEqual(api_comment['commenter']['user_id'],
                         db_comment['commenter']['user_id'])
        self.assertEqual(api_comment['comment_content'], db_comment['comment_content'])
        if with_reaction:
            self.assertEqual(api_comment['reactions']['count'],
                             db_comment['reactions']['count'])
            self.assertListEqual(api_comment['reactions']['type'],
                                 db_comment['reactions']['type'])

    def create_user(self):
        for user in self.USERS:
            User.objects.create(
                name="Praveen", profile_pic_url="None", password="None")

    def create_post(self):
        self.freeze.start()
        self.create_user()
        for post in self.POSTS:
            Post.objects.create(user_id=post['user_id'],
                                description=post['description'])

    def create_comment(self):
        self.freeze.start()
        self.create_post()
        for comment in self.COMMENTS:
            Comment.objects.create(user_id=self.foo_user.id, post_id=1,
                                   description="Testing comment from snapshot.")

    def create_comment_replies(self):
        self.freeze.start()
        # self.create_comment()
        for reply in self.REPLIES:
            Comment.objects.create(user_id=self.foo_user.id, parent_id=1,
                                   description="Testing Reply from snapshot.")

    def create_post_reaction(self):
        for reaction in self.POST_REACTIONS:
            Reaction.objects.create(
                user_id=self.foo_user.id, post_id=reaction['post_id'],
                reaction_type=reaction['reaction_type'])
            Reaction.objects.create(
                user_id=2, post_id=reaction['post_id'],
                reaction_type=reaction['reaction_type'])

    def create_comment_reaction(self):
        self.create_comment()
        for reaction in self.COMMENT_REACTIONS:
            Reaction.objects.create(
                user_id=self.foo_user.id, comment_id=reaction['comment_id'],
                reaction_type=reaction['reaction_type'])
            Reaction.objects.create(
                user_id=2, comment_id=reaction['comment_id'],
                reaction_type=reaction['reaction_type'])

    POST_SNAPSHOT = {
        "post_id": 1,
        "posted_by": {
            "name": "",
            "user_id": 1,
            "profile_pic_url": ""
        },
        "posted_at": "2012-01-14 12:00:01",
        "post_content": "Nothing",
        "reactions": {
            "count": 0,
            "type": []
        },
        "comments": [
            {
                "comment_id": 1,
                "commenter": {
                    "name": "",
                    "user_id": 1,
                    "profile_pic_url": ""
                },
                "commented_at": "2012-01-14 12:00:01",
                "comment_content": "Testing comment from snapshot.",
                "reactions": {
                    "count": 4,
                    "type": [
                        "HAHA",
                        "LIKE",
                        "LOVE",
                        "WOW"
                    ]
                },
                "replies_count": 5,
                "replies": [
                    {
                        "comment_id": 6,
                        "commenter": {
                            "name": "",
                            "user_id": 1,
                            "profile_pic_url": ""
                        },
                        "commented_at": "2012-01-14 12:00:01",
                        "comment_content": "Testing Reply from snapshot.",
                        "reactions": {
                            "count": 0,
                            "type": []
                        }
                    },
                    {
                        "comment_id": 7,
                        "commenter": {
                            "name": "",
                            "user_id": 1,
                            "profile_pic_url": ""
                        },
                        "commented_at": "2012-01-14 12:00:01",
                        "comment_content": "Testing Reply from snapshot.",
                        "reactions": {
                            "count": 0,
                            "type": []
                        }
                    },
                    {
                        "comment_id": 8,
                        "commenter": {
                            "name": "",
                            "user_id": 1,
                            "profile_pic_url": ""
                        },
                        "commented_at": "2012-01-14 12:00:01",
                        "comment_content": "Testing Reply from snapshot.",
                        "reactions": {
                            "count": 0,
                            "type": []
                        }
                    },
                    {
                        "comment_id": 9,
                        "commenter": {
                            "name": "",
                            "user_id": 1,
                            "profile_pic_url": ""
                        },
                        "commented_at": "2012-01-14 12:00:01",
                        "comment_content": "Testing Reply from snapshot.",
                        "reactions": {
                            "count": 0,
                            "type": []
                        }
                    },
                    {
                        "comment_id": 10,
                        "commenter": {
                            "name": "",
                            "user_id": 1,
                            "profile_pic_url": ""
                        },
                        "commented_at": "2012-01-14 12:00:01",
                        "comment_content": "Testing Reply from snapshot.",
                        "reactions": {
                            "count": 0,
                            "type": []
                        }
                    }
                ]
            },
            {
                "comment_id": 2,
                "commenter": {
                    "name": "",
                    "user_id": 1,
                    "profile_pic_url": ""
                },
                "commented_at": "2012-01-14 12:00:01",
                "comment_content": "Testing comment from snapshot.",
                "reactions": {
                    "count": 2,
                    "type": [
                        "LIKE",
                        "SAD"
                    ]
                },
                "replies_count": 0,
                "replies": []
            },
            {
                "comment_id": 3,
                "commenter": {
                    "name": "",
                    "user_id": 1,
                    "profile_pic_url": ""
                },
                "commented_at": "2012-01-14 12:00:01",
                "comment_content": "Testing comment from snapshot.",
                "reactions": {
                    "count": 1,
                    "type": [
                        "ANGRY"
                    ]
                },
                "replies_count": 0,
                "replies": []
            },
            {
                "comment_id": 4,
                "commenter": {
                    "name": "",
                    "user_id": 1,
                    "profile_pic_url": ""
                },
                "commented_at": "2012-01-14 12:00:01",
                "comment_content": "Testing comment from snapshot.",
                "reactions": {
                    "count": 0,
                    "type": []
                },
                "replies_count": 0,
                "replies": []
            },
            {
                "comment_id": 5,
                "commenter": {
                    "name": "",
                    "user_id": 1,
                    "profile_pic_url": ""
                },
                "commented_at": "2012-01-14 12:00:01",
                "comment_content": "Testing comment from snapshot.",
                "reactions": {
                    "count": 0,
                    "type": []
                },
                "replies_count": 0,
                "replies": []
            }
        ],
        "comments_count": 5
    }
    POSTS_REACTED_BY_USER = [1, 2, 3]
    POSTS_WITH_MORE_POSITIVE_REACTIONS = [1]
    REACTION_METRICS = [
        {"reaction_type": "ANGRY", "count": 2},
        {"reaction_type": "HAHA", "count": 2},
        {"reaction_type": "LIKE", "count": 2}
    ]
    POST_REACTIONS_WITH_USER = [
        {
            "name": "",
            "user_id": 1,
            "profile_pic_url": "",
            "reaction": "LIKE"
        },
        {
            "name": "Praveen",
            "user_id": 2,
            "profile_pic_url": "None",
            "reaction": "LIKE"
        },
        {
            "name": "",
            "user_id": 1,
            "profile_pic_url": "",
            "reaction": "SAD"
        },
        {
            "name": "Praveen",
            "user_id": 2,
            "profile_pic_url": "None",
            "reaction": "SAD"
        },
        {
            "name": "",
            "user_id": 1,
            "profile_pic_url": "",
            "reaction": "HAHA"
        },
        {
            "name": "Praveen",
            "user_id": 2,
            "profile_pic_url": "None",
            "reaction": "HAHA"
        },
        {
            "name": "",
            "user_id": 1,
            "profile_pic_url": "",
            "reaction": "ANGRY"
        },
        {
            "name": "Praveen",
            "user_id": 2,
            "profile_pic_url": "None",
            "reaction": "ANGRY"
        }
    ]
    TOTAL_REACTION_COUNT = {"reactions_count": 38}
    USER_POSTS_DATA = USER_POSTS
    COMMENT_REPLIES = [
        {
            "comment_id": 2,
            "commenter": {
                "name": "",
                "user_id": 1,
                "profile_pic_url": ""
            },
            "commented_at": "2019-08-24 12:30:33.645068",
            "comment_content": "Reply to the Comment1 no0"
        },
        {
            "comment_id": 3,
            "commenter": {
                "name": "",
                "user_id": 1,
                "profile_pic_url": ""
            },
            "commented_at": "2019-08-24 12:30:33.645223",
            "comment_content": "Reply to the Comment1 no1"
        },
        {
            "comment_id": 4,
            "commenter": {
                "name": "",
                "user_id": 1,
                "profile_pic_url": ""
            },
            "commented_at": "2019-08-24 12:30:33.645369",
            "comment_content": "Reply to the Comment1 no2"
        },
        {
            "comment_id": 5,
            "commenter": {
                "name": "",
                "user_id": 1,
                "profile_pic_url": ""
            },
            "commented_at": "2019-08-24 12:30:33.645513",
            "comment_content": "Reply to the Comment1 no3"
        },
        {
            "comment_id": 6,
            "commenter": {
                "name": "",
                "user_id": 1,
                "profile_pic_url": ""
            },
            "commented_at": "2019-08-24 12:30:33.645657",
            "comment_content": "Reply to the Comment1 no4"
        }
    ]
