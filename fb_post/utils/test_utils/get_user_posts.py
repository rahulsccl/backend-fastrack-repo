USER_POSTS = [
    {
        "post_id": 1,
        "posted_by": {
            "name": "",
            "user_id": 1,
            "profile_pic_url": ""
        },
        "posted_at": "2019-08-24 11:35:57.625518",
        "post_content": "Nothing",
        "reactions": {
            "count": 0,
            "type": []
        },
        "comments": [
            {
                "comment_id": 1,
                "commenter": {
                    "name": "",
                    "user_id": 1,
                    "profile_pic_url": ""
                },
                "commented_at": "2019-08-24 11:35:57.625967",
                "comment_content": "Testing comment from snapshot.",
                "reactions": {
                    "count": 4,
                    "type": [
                        "HAHA",
                        "LIKE",
                        "LOVE",
                        "WOW"
                    ]
                },
                "replies_count": 5,
                "replies": [
                    {
                        "comment_id": 6,
                        "commenter": {
                            "name": "",
                            "user_id": 1,
                            "profile_pic_url": ""
                        },
                        "commented_at": "2019-08-24 11:35:57.628833",
                        "comment_content": "Testing Reply from snapshot.",
                        "reactions": {
                            "count": 0,
                            "type": []
                        }
                    },
                    {
                        "comment_id": 7,
                        "commenter": {
                            "name": "",
                            "user_id": 1,
                            "profile_pic_url": ""
                        },
                        "commented_at": "2019-08-24 11:35:57.628963",
                        "comment_content": "Testing Reply from snapshot.",
                        "reactions": {
                            "count": 0,
                            "type": []
                        }
                    },
                    {
                        "comment_id": 8,
                        "commenter": {
                            "name": "",
                            "user_id": 1,
                            "profile_pic_url": ""
                        },
                        "commented_at": "2019-08-24 11:35:57.629092",
                        "comment_content": "Testing Reply from snapshot.",
                        "reactions": {
                            "count": 0,
                            "type": []
                        }
                    },
                    {
                        "comment_id": 9,
                        "commenter": {
                            "name": "",
                            "user_id": 1,
                            "profile_pic_url": ""
                        },
                        "commented_at": "2019-08-24 11:35:57.629219",
                        "comment_content": "Testing Reply from snapshot.",
                        "reactions": {
                            "count": 0,
                            "type": []
                        }
                    },
                    {
                        "comment_id": 10,
                        "commenter": {
                            "name": "",
                            "user_id": 1,
                            "profile_pic_url": ""
                        },
                        "commented_at": "2019-08-24 11:35:57.629347",
                        "comment_content": "Testing Reply from snapshot.",
                        "reactions": {
                            "count": 0,
                            "type": []
                        }
                    }
                ]
            },
            {
                "comment_id": 2,
                "commenter": {
                    "name": "",
                    "user_id": 1,
                    "profile_pic_url": ""
                },
                "commented_at": "2019-08-24 11:35:57.626155",
                "comment_content": "Testing comment from snapshot.",
                "reactions": {
                    "count": 2,
                    "type": [
                        "LIKE",
                        "SAD"
                    ]
                },
                "replies_count": 0,
                "replies": []
            },
            {
                "comment_id": 3,
                "commenter": {
                    "name": "",
                    "user_id": 1,
                    "profile_pic_url": ""
                },
                "commented_at": "2019-08-24 11:35:57.626290",
                "comment_content": "Testing comment from snapshot.",
                "reactions": {
                    "count": 1,
                    "type": [
                        "ANGRY"
                    ]
                },
                "replies_count": 0,
                "replies": []
            },
            {
                "comment_id": 4,
                "commenter": {
                    "name": "",
                    "user_id": 1,
                    "profile_pic_url": ""
                },
                "commented_at": "2019-08-24 11:35:57.626422",
                "comment_content": "Testing comment from snapshot.",
                "reactions": {
                    "count": 0,
                    "type": []
                },
                "replies_count": 0,
                "replies": []
            },
            {
                "comment_id": 5,
                "commenter": {
                    "name": "",
                    "user_id": 1,
                    "profile_pic_url": ""
                },
                "commented_at": "2019-08-24 11:35:57.626553",
                "comment_content": "Testing comment from snapshot.",
                "reactions": {
                    "count": 0,
                    "type": []
                },
                "replies_count": 0,
                "replies": []
            }
        ],
        "comments_count": 5
    },
    {
        "post_id": 2,
        "posted_by": {
            "name": "",
            "user_id": 1,
            "profile_pic_url": ""
        },
        "posted_at": "2019-08-24 11:35:57.625694",
        "post_content": "Nothing",
        "reactions": {
            "count": 0,
            "type": []
        },
        "comments": [],
        "comments_count": 0
    }
]