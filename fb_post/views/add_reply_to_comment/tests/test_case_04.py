"""
# TODO: Update test case description
# DESCRIPTION: In this test case we are checking the test with invalid comment id
                * Failure
                    * Invalid Comment Id

"""
import json

from django_swagger_utils.utils.test import CustomAPITestCase

from fb_post.constants.exception_messsages import NOT_FOUND, INVALID_COMMENT_ID
from fb_post.models import Post, Comment
from . import APP_NAME, OPERATION_NAME, REQUEST_METHOD, URL_SUFFIX

REQUEST_BODY = """
{
    "comment_description": "string"
}
"""

TEST_CASE = {
    "request": {
        "path_params": {"id": 1123},
        "query_params": {},
        "header_params": {},
        "securities": {"oauth": {"tokenUrl": "http://auth.ibtspl.com/oauth2/", "flow": "password", "scopes": ["superuser"], "type": "oauth2"}},
        "body": REQUEST_BODY,
    },
}


class TestCase04AddReplyToCommentAPITestCase(CustomAPITestCase):
    app_name = APP_NAME
    operation_name = OPERATION_NAME
    request_method = REQUEST_METHOD
    url_suffix = URL_SUFFIX
    test_case_dict = TEST_CASE

    def setupUser(self, username, password):
        super(TestCase04AddReplyToCommentAPITestCase, self).setupUser(
            username=username,
            password=password
        )
        Post.objects.create(user_id=self.foo_user.id, description="Nothing to post here.")
        Comment.objects.create(user_id=self.foo_user.id,
                               post_id=1, description="Comment to post.")

    def test_case(self):
        self.default_test_case()
