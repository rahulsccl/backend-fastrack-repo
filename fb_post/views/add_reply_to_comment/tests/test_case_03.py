"""
# TODO: Update test case description
# DESCRIPTION: In this test case we are checking the test with valid input
            * Success
                * Check Response Code
                * Check Reply Description
"""

from django_swagger_utils.utils.test import CustomAPITestCase

from fb_post.constants.exception_messsages import ACCEPTED
from fb_post.models import Post, Comment
from . import APP_NAME, OPERATION_NAME, REQUEST_METHOD, URL_SUFFIX

REQUEST_BODY = """
{
    "comment_description": "Reply to the comment"
}
"""

TEST_CASE = {
    "request": {
        "path_params": {"id": 1},
        "query_params": {},
        "header_params": {},
        "securities": {"oauth": {"tokenUrl": "http://auth.ibtspl.com/oauth2/", "flow": "password", "scopes": ["superuser"], "type": "oauth2"}},
        "body": REQUEST_BODY,
    },
}


class TestCase03AddReplyToCommentAPITestCase(CustomAPITestCase):
    app_name = APP_NAME
    operation_name = OPERATION_NAME
    request_method = REQUEST_METHOD
    url_suffix = URL_SUFFIX
    test_case_dict = TEST_CASE

    def setupUser(self, username, password):
        super(TestCase03AddReplyToCommentAPITestCase, self).setupUser(
            username=username,
            password=password
        )
        Post.objects.create(user_id=self.foo_user.id, description="Nothing to post here.")
        Comment.objects.create(user_id=self.foo_user.id,
                               post_id=1, description="Comment to post.")

    def test_case(self):
        import json
        response = self.default_test_case()

        # check comment id
        # check user id
        # check comment description

        comment_id_from_response = json.loads(response.content)['comment_id']
        comment = Comment.objects.get(id=comment_id_from_response, user_id=self.foo_user.id)

        self.assert_match_snapshot(
            value=comment.description,
            name='comment_description'
        )

        self.assert_match_snapshot(
            value=comment.parent_id,
            name='parent_id'
        )