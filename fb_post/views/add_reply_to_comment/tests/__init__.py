# pylint: disable=wrong-import-position

APP_NAME = "fb_post"
OPERATION_NAME = "add_reply_to_comment"
REQUEST_METHOD = "post"
URL_SUFFIX = "comment/{id}/reply/v1/"

from .test_case_01 import TestCase01AddReplyToCommentAPITestCase
from .test_case_02 import TestCase02AddReplyToCommentAPITestCase
from .test_case_03 import TestCase03AddReplyToCommentAPITestCase
from .test_case_04 import TestCase04AddReplyToCommentAPITestCase
from .test_case_05 import TestCase05AddReplyToCommentAPITestCase

__all__ = [
    "TestCase01AddReplyToCommentAPITestCase",
    "TestCase02AddReplyToCommentAPITestCase",
    "TestCase03AddReplyToCommentAPITestCase",
    "TestCase04AddReplyToCommentAPITestCase",
    "TestCase05AddReplyToCommentAPITestCase"
]
