from django_swagger_utils.drf_server.exceptions import NotFound
from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_post.constants.exception_messsages import INVALID_COMMENT_ID
from fb_post.utils.utils import reply_to_comment, InvalidCommentId
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    comment_id = kwargs['comment_id']
    reply_text = kwargs['request_data']['reply_text']
    user_id = kwargs['user'].id

    import json
    from django.http.response import HttpResponse
    try:
        reply_id = reply_to_comment(comment_id, user_id, reply_text)
    except InvalidCommentId:
        raise NotFound(*INVALID_COMMENT_ID)

    response_data = json.dumps(
        {
            "reply_id": reply_id
        }
    )
    return HttpResponse(response_data, status=201)
