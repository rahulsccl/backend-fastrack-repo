# pylint: disable=wrong-import-position

APP_NAME = "fb_post"
OPERATION_NAME = "get_post"
REQUEST_METHOD = "get"
URL_SUFFIX = "post/{id}/v1/"

from .test_case_01 import TestCase01GetPostAPITestCase
from .test_case_02 import TestCase02GetPostAPITestCase
from .test_case_03 import TestCase03GetPostAPITestCase
from .test_case_04 import TestCase04GetPostAPITestCase

__all__ = [
    "TestCase01GetPostAPITestCase",
    "TestCase02GetPostAPITestCase",
    "TestCase03GetPostAPITestCase",
    "TestCase04GetPostAPITestCase"
]
