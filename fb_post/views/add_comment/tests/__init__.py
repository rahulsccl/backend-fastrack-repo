# pylint: disable=wrong-import-position

APP_NAME = "fb_post"
OPERATION_NAME = "add_comment"
REQUEST_METHOD = "post"
URL_SUFFIX = "post/{id}/comment/v1/"

from .test_case_01 import TestCase01AddCommentAPITestCase
from .test_case_02 import TestCase02AddCommentAPITestCase
from .test_case_03 import TestCase03AddCommentAPITestCase
from .test_case_04 import TestCase04AddCommentAPITestCase

__all__ = [
    "TestCase01AddCommentAPITestCase",
    "TestCase02AddCommentAPITestCase",
    "TestCase03AddCommentAPITestCase",
    "TestCase04AddCommentAPITestCase"
]
