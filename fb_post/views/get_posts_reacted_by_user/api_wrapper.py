from django.http import HttpResponse
from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_post.utils.utils import get_posts_reacted_by_user, InvalidUserId
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    user_id = kwargs['user'].id
    query_parameters = kwargs['request_query_params']

    offset = query_parameters.offset
    limit = query_parameters.limit

    reacted_posts = get_posts_reacted_by_user(user_id, offset, limit)

    import json
    from django.http import HttpResponse

    response_data = json.dumps(reacted_posts)
    return HttpResponse(response_data, status=200)