# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['TestCase06AddReactionToPostAPITestCase::test_case status'] = 202

snapshots['TestCase06AddReactionToPostAPITestCase::test_case body'] = {
    'reaction_type': 'LIKE'
}

snapshots['TestCase06AddReactionToPostAPITestCase::test_case header_params'] = {
    'content-language': [
        'Content-Language',
        'en'
    ],
    'content-length': [
        '25',
        'Content-Length'
    ],
    'content-type': [
        'Content-Type',
        'text/html; charset=utf-8'
    ],
    'vary': [
        'Accept-Language, Origin, Cookie',
        'Vary'
    ],
    'x-frame-options': [
        'SAMEORIGIN',
        'X-Frame-Options'
    ]
}

snapshots['TestCase06AddReactionToPostAPITestCase::test_case reacted_user_id'] = 1

snapshots['TestCase06AddReactionToPostAPITestCase::test_case reacted_post_id'] = 1
