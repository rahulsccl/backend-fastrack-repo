"""
# TODO: Complete all three cases for this one
# TODO: Update test case description
# DESCRIPTION: In this test case we are checking the test with valid input and
                reaction is inserting or not.

"""

from django_swagger_utils.utils.test import CustomAPITestCase

from fb_post.constants.exception_messsages import ACCEPTED
from fb_post.models import Post
from fb_post.utils.test_utils.custom_test_util import TestFixtures
from . import APP_NAME, OPERATION_NAME, REQUEST_METHOD, URL_SUFFIX

REQUEST_BODY = """
{
    "reaction_type": "LIKE"
}
"""

TEST_CASE = {
    "request": {
        "path_params": {"id": 1},
        "query_params": {},
        "header_params": {},
        "securities": {
            "oauth": {"tokenUrl": "http://auth.ibtspl.com/oauth2/", "flow": "password",
                      "scopes": ["superuser"], "type": "oauth2"}},
        "body": REQUEST_BODY,
    },
}


class TestCase01AddReactionToPostAPITestCase(TestFixtures):
    app_name = APP_NAME
    operation_name = OPERATION_NAME
    request_method = REQUEST_METHOD
    url_suffix = URL_SUFFIX
    test_case_dict = TEST_CASE

    def setupUser(self, username, password):
        super(TestCase01AddReactionToPostAPITestCase, self).setupUser(
            username=username,
            password=password
        )
        Post.objects.create(user_id=self.foo_user.id, description="Nothing to post here.")

    def test_case(self):
        import json
        response = self.default_test_case()
        response_code = response.status_code
        reaction_type = json.loads(response.content)['reaction_type']

        post = Post.objects.get(id=1)
        reaction = post.reactions.all()[:1]

        self.assertEqual(response_code, ACCEPTED)
        self.assertEqual(reaction_type, reaction[0].reaction_type)
