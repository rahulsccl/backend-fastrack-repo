from django_swagger_utils.drf_server.exceptions import NotFound, BadRequest
from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_post.constants.exception_messsages import INVALID_REACTION, \
    INVALID_POST_ID
from fb_post.utils.utils import react_to_post, InvalidReaction, InvalidPostId
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    post_id = kwargs['post_id']
    reaction_type = kwargs['request_data']['reaction_type']
    user_id = kwargs['user'].id

    from django.http.response import HttpResponse
    try:
        react_to_post(user_id, post_id, reaction_type)
    except InvalidPostId:
        raise NotFound(*INVALID_POST_ID)
    except InvalidReaction:
        raise BadRequest(*INVALID_REACTION)
    return HttpResponse(status=200)
