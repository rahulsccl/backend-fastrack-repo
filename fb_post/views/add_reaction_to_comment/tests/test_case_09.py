"""
# TODO: Update test case description
# DESCRIPTION: In this test case we are checking the test with valid input update
            * Success
                * Check Response Code
                * Check Reaction Type
"""

from django_swagger_utils.utils.test import CustomAPITestCase

from fb_post.constants.exception_messsages import ACCEPTED
from fb_post.models import Post, Comment, Reaction
from . import APP_NAME, OPERATION_NAME, REQUEST_METHOD, URL_SUFFIX

REQUEST_BODY = """
{
    "reaction_type": "LIKE"
}
"""

# TODO: Complete all three cases for this one

TEST_CASE = {
    "request": {
        "path_params": {"id": 1},
        "query_params": {},
        "header_params": {},
        "securities": {"oauth": {"tokenUrl": "http://auth.ibtspl.com/oauth2/", "flow": "password", "scopes": ["superuser"], "type": "oauth2"}},
        "body": REQUEST_BODY,
    },
}


class TestCase09AddReactionToCommentAPITestCase(CustomAPITestCase):
    app_name = APP_NAME
    operation_name = OPERATION_NAME
    request_method = REQUEST_METHOD
    url_suffix = URL_SUFFIX
    test_case_dict = TEST_CASE

    def setupUser(self, username, password):
        super(TestCase09AddReactionToCommentAPITestCase, self).setupUser(
            username=username,
            password=password
        )
        Post.objects.create(user_id=self.foo_user.id, description="Nothing to post here.")
        comment = Comment.objects.create(user_id=self.foo_user.id,
                               post_id=1, description="Comment to post.")
        Reaction.objects.create(user_id=self.foo_user.id,
                                comment_id=comment.id, reaction_type="LOVE")

    def test_case(self):
        import json
        response = self.default_test_case()

        # check updated reaction type
        # check user id
        # check comment id

        reaction_type_from_response = json.loads(response.content)['reaction_type']
        reaction = Reaction.objects.filter(reaction_type=reaction_type_from_response)[0]

        self.assert_match_snapshot(
            name='reacted_user_id',
            value=reaction.user_id
        )

        self.assert_match_snapshot(
            name='reacted_comment_id',
            value=reaction.comment_id
        )