from django_swagger_utils.drf_server.exceptions import NotFound
from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_post.constants.exception_messsages import INVALID_REACTION, \
    INVALID_COMMENT_ID
from fb_post.utils.utils import InvalidReaction, \
    InvalidCommentId, react_to_comment
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    comment_id = kwargs['comment_id']
    reaction_type = kwargs['request_data']['reaction_type']
    user_id = kwargs['user'].id

    from django.http.response import HttpResponse
    try:
        react_to_comment(user_id, comment_id, reaction_type)
    except InvalidCommentId:
        raise NotFound(*INVALID_COMMENT_ID)
    except InvalidReaction:
        raise NotFound(*INVALID_REACTION)
    return HttpResponse(status=200)
