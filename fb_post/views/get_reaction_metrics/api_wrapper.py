import json

from django.http import HttpResponse
from django_swagger_utils.drf_server.exceptions import NotFound
from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_post.constants.exception_messsages import INVALID_POST_ID
from fb_post.utils.utils import get_reaction_metrics, InvalidPostId
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    post_id = kwargs['id']

    try:
        reaction_metrics_list = get_reaction_metrics(post_id)
    except InvalidPostId:
        raise NotFound(*INVALID_POST_ID)

    response_data = json.dumps(reaction_metrics_list)
    return HttpResponse(response_data, status=200)