"""
# DESCRIPTION: In this test case we are checking the test with valid input
                *Success
                    * Check Response code
                    * Check Description of the Post
"""

from django_swagger_utils.utils.test import CustomAPITestCase

from fb_post.constants.exception_messsages import ACCEPTED
from fb_post.models import Post
from . import APP_NAME, OPERATION_NAME, REQUEST_METHOD, URL_SUFFIX

REQUEST_BODY = """
{
    "post_content": "This is the new post."
}
"""

TEST_CASE = {
    "request": {
        "path_params": {},
        "query_params": {},
        "header_params": {},
        "securities": {
            "oauth": {"tokenUrl": "http://auth.ibtspl.com/oauth2/", "flow": "password",
                      "scopes": ["superuser"], "type": "oauth2"}},
        "body": REQUEST_BODY,
    },
}


class TestCase01AddPostAPITestCase(CustomAPITestCase):
    app_name = APP_NAME
    operation_name = OPERATION_NAME
    request_method = REQUEST_METHOD
    url_suffix = URL_SUFFIX
    test_case_dict = TEST_CASE

    def setupUser(self, username, password):
        super(TestCase01AddPostAPITestCase, self).setupUser(
            username=username,
            password=password
        )

    def test_case(self):
        import json
        response = self.default_test_case()
        post_id = json.loads(response.content)["post_id"]
        status_code = response.status_code

        post = Post.objects.get(id=post_id)
        api_post_description = "This is the new post."

        self.assertEqual(status_code, ACCEPTED)
        self.assertEqual(api_post_description, post.description)
