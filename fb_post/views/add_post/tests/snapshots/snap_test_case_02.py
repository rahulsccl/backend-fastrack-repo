# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['TestCase02AddPostAPITestCase::test_case status'] = 202

snapshots['TestCase02AddPostAPITestCase::test_case body'] = {
    'post_id': 1
}

snapshots['TestCase02AddPostAPITestCase::test_case header_params'] = {
    'content-language': [
        'Content-Language',
        'en'
    ],
    'content-length': [
        '14',
        'Content-Length'
    ],
    'content-type': [
        'Content-Type',
        'text/html; charset=utf-8'
    ],
    'vary': [
        'Accept-Language, Origin, Cookie',
        'Vary'
    ],
    'x-frame-options': [
        'SAMEORIGIN',
        'X-Frame-Options'
    ]
}

snapshots['TestCase02AddPostAPITestCase::test_case post_id'] = 1

snapshots['TestCase02AddPostAPITestCase::test_case post_description'] = 'This is the new post.'

snapshots['TestCase02AddPostAPITestCase::test_case user_id'] = 1
