# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['TestCase03GetUserPostsAPITestCase::test_case status'] = 200

snapshots['TestCase03GetUserPostsAPITestCase::test_case body'] = {
    'result': [
        {
            'comments': [
                {
                    'comment_content': 'Testing comment from snapshot.',
                    'comment_id': 1,
                    'commented_at': '2012-01-14 12:00:01',
                    'commenter': {
                        'name': '',
                        'profile_pic_url': '',
                        'user_id': 1
                    },
                    'reactions': {
                        'count': 4,
                        'type': [
                            'HAHA',
                            'LIKE',
                            'LOVE',
                            'WOW'
                        ]
                    },
                    'replies': [
                        {
                            'comment_content': 'Testing Reply from snapshot.',
                            'comment_id': 6,
                            'commented_at': '2012-01-14 12:00:01',
                            'commenter': {
                                'name': '',
                                'profile_pic_url': '',
                                'user_id': 1
                            },
                            'reactions': {
                                'count': 0,
                                'type': [
                                ]
                            }
                        },
                        {
                            'comment_content': 'Testing Reply from snapshot.',
                            'comment_id': 7,
                            'commented_at': '2012-01-14 12:00:01',
                            'commenter': {
                                'name': '',
                                'profile_pic_url': '',
                                'user_id': 1
                            },
                            'reactions': {
                                'count': 0,
                                'type': [
                                ]
                            }
                        },
                        {
                            'comment_content': 'Testing Reply from snapshot.',
                            'comment_id': 8,
                            'commented_at': '2012-01-14 12:00:01',
                            'commenter': {
                                'name': '',
                                'profile_pic_url': '',
                                'user_id': 1
                            },
                            'reactions': {
                                'count': 0,
                                'type': [
                                ]
                            }
                        },
                        {
                            'comment_content': 'Testing Reply from snapshot.',
                            'comment_id': 9,
                            'commented_at': '2012-01-14 12:00:01',
                            'commenter': {
                                'name': '',
                                'profile_pic_url': '',
                                'user_id': 1
                            },
                            'reactions': {
                                'count': 0,
                                'type': [
                                ]
                            }
                        },
                        {
                            'comment_content': 'Testing Reply from snapshot.',
                            'comment_id': 10,
                            'commented_at': '2012-01-14 12:00:01',
                            'commenter': {
                                'name': '',
                                'profile_pic_url': '',
                                'user_id': 1
                            },
                            'reactions': {
                                'count': 0,
                                'type': [
                                ]
                            }
                        }
                    ],
                    'replies_count': 5
                },
                {
                    'comment_content': 'Testing comment from snapshot.',
                    'comment_id': 2,
                    'commented_at': '2012-01-14 12:00:01',
                    'commenter': {
                        'name': '',
                        'profile_pic_url': '',
                        'user_id': 1
                    },
                    'reactions': {
                        'count': 2,
                        'type': [
                            'LIKE',
                            'SAD'
                        ]
                    },
                    'replies': [
                    ],
                    'replies_count': 0
                },
                {
                    'comment_content': 'Testing comment from snapshot.',
                    'comment_id': 3,
                    'commented_at': '2012-01-14 12:00:01',
                    'commenter': {
                        'name': '',
                        'profile_pic_url': '',
                        'user_id': 1
                    },
                    'reactions': {
                        'count': 1,
                        'type': [
                            'ANGRY'
                        ]
                    },
                    'replies': [
                    ],
                    'replies_count': 0
                },
                {
                    'comment_content': 'Testing comment from snapshot.',
                    'comment_id': 4,
                    'commented_at': '2012-01-14 12:00:01',
                    'commenter': {
                        'name': '',
                        'profile_pic_url': '',
                        'user_id': 1
                    },
                    'reactions': {
                        'count': 0,
                        'type': [
                        ]
                    },
                    'replies': [
                    ],
                    'replies_count': 0
                },
                {
                    'comment_content': 'Testing comment from snapshot.',
                    'comment_id': 5,
                    'commented_at': '2012-01-14 12:00:01',
                    'commenter': {
                        'name': '',
                        'profile_pic_url': '',
                        'user_id': 1
                    },
                    'reactions': {
                        'count': 0,
                        'type': [
                        ]
                    },
                    'replies': [
                    ],
                    'replies_count': 0
                }
            ],
            'comments_count': 5,
            'post_content': 'Nothing',
            'post_id': 1,
            'posted_at': '2012-01-14 12:00:01',
            'posted_by': {
                'name': '',
                'profile_pic_url': '',
                'user_id': 1
            },
            'reactions': {
                'count': 0,
                'type': [
                ]
            }
        },
        {
            'comments': [
            ],
            'comments_count': 0,
            'post_content': 'Nothing',
            'post_id': 2,
            'posted_at': '2012-01-14 12:00:01',
            'posted_by': {
                'name': '',
                'profile_pic_url': '',
                'user_id': 1
            },
            'reactions': {
                'count': 0,
                'type': [
                ]
            }
        }
    ],
    'total': 2
}

snapshots['TestCase03GetUserPostsAPITestCase::test_case header_params'] = {
    'content-language': [
        'Content-Language',
        'en'
    ],
    'content-length': [
        '2813',
        'Content-Length'
    ],
    'content-type': [
        'Content-Type',
        'text/html; charset=utf-8'
    ],
    'vary': [
        'Accept-Language, Origin, Cookie',
        'Vary'
    ],
    'x-frame-options': [
        'SAMEORIGIN',
        'X-Frame-Options'
    ]
}
