# pylint: disable=wrong-import-position

APP_NAME = "fb_post"
OPERATION_NAME = "get_user_posts"
REQUEST_METHOD = "get"
URL_SUFFIX = "user/{id}/posts/v1/"

from .test_case_01 import TestCase01GetUserPostsAPITestCase
from .test_case_02 import TestCase02GetUserPostsAPITestCase
from .test_case_03 import TestCase03GetUserPostsAPITestCase
from .test_case_04 import TestCase04GetUserPostsAPITestCase
from .test_case_05 import TestCase05GetUserPostsAPITestCase
from .test_case_06 import TestCase06GetUserPostsAPITestCase
from .test_case_07 import TestCase07GetUserPostsAPITestCase

__all__ = [
    "TestCase01GetUserPostsAPITestCase",
    "TestCase02GetUserPostsAPITestCase",
    "TestCase03GetUserPostsAPITestCase",
    "TestCase04GetUserPostsAPITestCase",
    "TestCase05GetUserPostsAPITestCase",
    "TestCase06GetUserPostsAPITestCase",
    "TestCase07GetUserPostsAPITestCase"
]
