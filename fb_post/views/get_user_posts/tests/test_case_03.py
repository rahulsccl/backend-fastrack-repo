"""
# TODO: Update test case description
# DESCRIPTION: In this test case I checked following things
                * Check Response Code ✔
                * Check Posts Id ✔
                * Check Reaction Count ✔
                * Check Comment Count ✔
                * Check Replies Count ✔
"""
import json

from django.db.models import Prefetch
from django_swagger_utils.utils.test import CustomAPITestCase

from fb_post.constants.exception_messsages import get_random_reaction, OK
from fb_post.models import Post, Comment, Reaction
from fb_post.utils.test_utils.custom_test_util import TestFixtures
from fb_post.views.get_post.tests import TestCase01GetPostAPITestCase
from . import APP_NAME, OPERATION_NAME, REQUEST_METHOD, URL_SUFFIX

REQUEST_BODY = """

"""

TEST_CASE = {
    "request": {
        "path_params": {"id": 1},
        "query_params": {
            "offset": 0,
            "limit": 2
        },
        "header_params": {},
        "securities": {"oauth": {"tokenUrl": "http://auth.ibtspl.com/oauth2/", "flow": "password", "scopes": ["superuser"], "type": "oauth2"}},
        "body": REQUEST_BODY,
    },
}


class TestCase03GetUserPostsAPITestCase(TestFixtures):
    app_name = APP_NAME
    operation_name = OPERATION_NAME
    request_method = REQUEST_METHOD
    url_suffix = URL_SUFFIX
    test_case_dict = TEST_CASE

    def setupUser(self, username, password):
        super(TestCase03GetUserPostsAPITestCase, self).setupUser(
            username=username,
            password=password
        )

        self.create_comment_reaction()
        self.create_comment_replies()

    def test_case(self):
        self.default_test_case()