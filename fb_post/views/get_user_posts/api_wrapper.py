from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_post.constants.exception_messsages import INVALID_USER_ID_RESPONSE
from fb_post.utils.utils import get_user_posts, InvalidUserId
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    user_id = kwargs['user'].id
    query_parameters = kwargs['request_query_params']
    offset = query_parameters.offset
    limit = query_parameters.limit

    import json
    from django.http.response import HttpResponse
    try:
        user_posts = get_user_posts(user_id, offset, limit)
    except InvalidUserId:
        return HttpResponse(INVALID_USER_ID_RESPONSE, status=404)

    response_data = json.dumps(user_posts)
    return HttpResponse(response_data, status=200)
