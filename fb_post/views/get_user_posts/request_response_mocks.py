


RESPONSE_200_JSON = """
{
    "total": 1,
    "result": [
        {
            "post_id": 1.1,
            "post_content": "string",
            "posted_at": "2099-12-31 00:00:00",
            "posted_by": {
                "name": "string",
                "profile_pic_url": "string"
            },
            "reactions": {
                "type": [
                    {
                        "type": "LIKE"
                    }
                ],
                "count": 1.1
            },
            "comment_with_replies": [
                {
                    "commenter": {
                        "name": "string",
                        "profile_pic_url": "string"
                    },
                    "comment_content": "string",
                    "comment_id": 1.1,
                    "reactions": {
                        "type": [
                            {
                                "type": "LIKE"
                            }
                        ],
                        "count": 1.1
                    },
                    "total": 1,
                    "result": [
                        {
                            "commenter": {
                                "name": "string",
                                "profile_pic_url": "string"
                            },
                            "comment_content": "string",
                            "comment_id": 1.1,
                            "reactions": {
                                "type": [
                                    {
                                        "type": "LIKE"
                                    }
                                ],
                                "count": 1.1
                            },
                            "name": "string",
                            "profile_pic_url": "string",
                            "reaction_type": "LIKE"
                        }
                    ]
                }
            ]
        }
    ]
}
"""

