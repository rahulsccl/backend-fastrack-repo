# pylint: disable=wrong-import-position

APP_NAME = "fb_post"
OPERATION_NAME = "get_reactions_to_post"
REQUEST_METHOD = "get"
URL_SUFFIX = "post/{id}/reactions/v1/"

from .test_case_01 import TestCase01GetReactionsToPostAPITestCase
from .test_case_02 import TestCase02GetReactionsToPostAPITestCase
from .test_case_03 import TestCase03GetReactionsToPostAPITestCase
from .test_case_04 import TestCase04GetReactionsToPostAPITestCase
from .test_case_05 import TestCase05GetReactionsToPostAPITestCase
from .test_case_06 import TestCase06GetReactionsToPostAPITestCase
from .test_case_07 import TestCase07GetReactionsToPostAPITestCase

__all__ = [
    "TestCase01GetReactionsToPostAPITestCase",
    "TestCase02GetReactionsToPostAPITestCase",
    "TestCase03GetReactionsToPostAPITestCase",
    "TestCase04GetReactionsToPostAPITestCase",
    "TestCase05GetReactionsToPostAPITestCase",
    "TestCase06GetReactionsToPostAPITestCase",
    "TestCase07GetReactionsToPostAPITestCase"
]
