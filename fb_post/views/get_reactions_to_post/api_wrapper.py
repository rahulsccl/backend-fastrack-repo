import json

from django.http import HttpResponse
from django_swagger_utils.drf_server.exceptions import BadRequest
from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_post.constants.exception_messsages import INVALID_POST_ID, \
    INVALID_POST_ID_RESPONSE
from fb_post.utils.utils import get_reactions_to_post, InvalidPostId
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    post_id = kwargs['post_id']
    query_parameters = kwargs['request_query_params']
    offset = query_parameters.offset
    limit = query_parameters.limit

    try:
        post_reactions = get_reactions_to_post(post_id, offset, limit)
    except InvalidPostId:
        return HttpResponse(INVALID_POST_ID_RESPONSE, status=404)
    response_data = json.dumps(
        post_reactions
    )
    return HttpResponse(response_data, status=200)