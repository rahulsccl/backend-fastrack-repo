from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import ValidatorAbstractClass

from fb_post.utils.test_utils.validate_query import ValidateQuery


class ValidatorClass(ValidatorAbstractClass, ValidateQuery):
    def __init__(self, *args, **kwargs):
        self.request_data = kwargs['request_data']
        self.user = kwargs['user']
        self.user_dto = kwargs['user_dto']
        self.access_token = kwargs['access_token']
        self.query_parameters = kwargs['request_query_params']

    def validate(self):
        """
        A wrapper function that calls all the validations and sends back
        necessary data.
        :return: A dictionary with values that are to be carry-forwarded to
        api_wrapper.
        """
        dict_ = dict()
        dict_['some_key'] = self.validate_offset(self.query_parameters.offset)
        dict_['other_key'] = self.validate_limit(self.query_parameters.limit)
        return dict_
