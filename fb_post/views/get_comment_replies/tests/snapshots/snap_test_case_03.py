# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['TestCase03GetCommentRepliesAPITestCase::test_case status'] = 200

snapshots['TestCase03GetCommentRepliesAPITestCase::test_case body'] = {
    'result': [
        {
            'comment_content': 'Reply to the Comment1 no0',
            'comment_id': 2,
            'commented_at': '2012-01-14 12:00:01',
            'commenter': {
                'name': '',
                'profile_pic_url': '',
                'user_id': 1
            }
        },
        {
            'comment_content': 'Reply to the Comment1 no1',
            'comment_id': 3,
            'commented_at': '2012-01-14 12:00:01',
            'commenter': {
                'name': '',
                'profile_pic_url': '',
                'user_id': 1
            }
        },
        {
            'comment_content': 'Reply to the Comment1 no2',
            'comment_id': 4,
            'commented_at': '2012-01-14 12:00:01',
            'commenter': {
                'name': '',
                'profile_pic_url': '',
                'user_id': 1
            }
        }
    ],
    'total': 5
}

snapshots['TestCase03GetCommentRepliesAPITestCase::test_case header_params'] = {
    'content-language': [
        'Content-Language',
        'en'
    ],
    'content-length': [
        '534',
        'Content-Length'
    ],
    'content-type': [
        'Content-Type',
        'text/html; charset=utf-8'
    ],
    'vary': [
        'Accept-Language, Origin, Cookie',
        'Vary'
    ],
    'x-frame-options': [
        'SAMEORIGIN',
        'X-Frame-Options'
    ]
}
