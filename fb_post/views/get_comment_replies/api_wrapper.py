from django_swagger_utils.drf_server.exceptions import NotFound
from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_post.constants.exception_messsages import INVALID_COMMENT_ID
from fb_post.utils.utils import get_replies_for_comment, InvalidCommentId
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    comment_id = kwargs['comment_id']
    query_parameters = kwargs['request_query_params']

    offset = query_parameters.offset
    limit = query_parameters.limit

    import json
    from django.http import HttpResponse
    try:
        comment_replies = get_replies_for_comment(comment_id, offset, limit)
    except InvalidCommentId:
        raise NotFound(*INVALID_COMMENT_ID)

    response_data = json.dumps(comment_replies)
    return HttpResponse(response_data, status=200)
