


RESPONSE_202_JSON = """
{
    "total": 1,
    "result": [
        {
            "commenter": {
                "name": "string",
                "profile_pic_url": "string"
            },
            "comment_content": "string",
            "comment_id": 1.1,
            "reactions": {
                "type": [
                    {
                        "type": "LIKE"
                    }
                ],
                "count": 1.1
            },
            "name": "string",
            "profile_pic_url": "string",
            "reaction_type": "LIKE"
        }
    ]
}
"""

