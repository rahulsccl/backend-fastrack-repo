from typing import Generator, Callable


# function
def gcd(a: int, b: int) -> int:
    while b:
        a, b = b, a % b
    return a


def fun(cb: Callable[[int, int], int]) -> int:
    return cb(55, 66)


# lambda
f: Callable[[int, int], int] = lambda x: x * 2
print(f)
print(gcd(10,20))