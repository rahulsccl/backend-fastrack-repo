import json
import random

from fb_post.constants.reactions import ReactionType

OK = 200
ACCEPTED = 202
BAD_REQUEST = 400
UN_AUTHORIZED = 401
FORBIDDEN = 403
NOT_FOUND = 404

positive_reaction_types = [ReactionType.LIKE.value, ReactionType.LOVE.value,
                               ReactionType.HAHA.value, ReactionType.WOW.value]
negative_reaction_types = [ReactionType.WOW.value, ReactionType.ANGRY.value]

INVALID_POST_CONTENT = (
    "INVALID_POST_CONTENT",
    "Post content length should be less than or equal to 5"
)
INVALID_POST_ID = (
    "POST_NOT_EXIST",
    "Post is not exist in our database"
)
INVALID_USER_ID = (
    "USER_NOT_EXIST",
    "User is not exist in our database"
)
INVALID_COMMENT_ID = (
    "COMMENT_NOT_EXIST",
    "Comment is not exist in our database"
)
INVALID_REACTION = (
    "REACTION_NOT_ALLOWED",
    "Given reaction is not allowed to react."
)
INVALID_POST_ID_RESPONSE = json.dumps(
    {
        "response": "POST_NOT_EXIST",
        "http_status_code": 404,
        "res_status": "Post is not exist in our database"
    }
)
OFFSET_LIMIT_NEGATIVE_NUMBER = (
    "NEGATIVE_NUMBER_NOT_ALLOWED",
    "You cannot request with negative numbers."
)
HIGH_LIMIT_ERROR = (
    "GREATER_THAN_100",
    "You cannot request more than 100."
)
INVALID_USER_ID_RESPONSE = json.dumps(
    {
        "response": "USER_NOT_EXIST",
        "http_status_code": 404,
        "res_status": "User is not exist in our database"
    }
)
INVALID_COMMENT_ID_RESPONSE = json.dumps(
    {
        "response": "COMMENT_NOT_EXIST",
        "http_status_code": 404,
        "res_status": "Comment is not exist in our database"
    }
)


def get_random_reaction():
    reactions = ["LIKE", "LOVE", "WOW", "HAHA", "SAD", "ANGRY"]
    return random.choice(reactions)
