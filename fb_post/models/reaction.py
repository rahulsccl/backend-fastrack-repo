from django.db import models

from fb_post.models.comment import Comment
from fb_post.models.post import Post
from fb_post.models.user import User
from ..constants.reactions import ReactionType


class Reaction(models.Model):
    reaction = [
        (ReactionType.LIKE.value, ReactionType.LIKE.value),
        (ReactionType.LOVE.value, ReactionType.LOVE.value),
        (ReactionType.HAHA.value, ReactionType.HAHA.value),
        (ReactionType.WOW.value, ReactionType.WOW.value),
        (ReactionType.SAD.value, ReactionType.SAD.value),
        (ReactionType.ANGRY.value, ReactionType.ANGRY.value)
    ]
    reaction_type = models.CharField(max_length=5, choices=reaction,
                                     default=ReactionType.LIKE.value)
    comment = models.ForeignKey(Comment, on_delete=models.CASCADE, null=True,
                                default=None, blank=True,
                                related_name="reactions")
    post = models.ForeignKey(Post, on_delete=models.CASCADE, null=True,
                             default=None, blank=True,
                             related_name="reactions")
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                             related_name="reactions")

    def __str__(self):
        return self.reaction_type
